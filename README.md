[![Crates.io](https://img.shields.io/crates/v/trompt.svg)](https://crates.io/crates/trompt)
[![build status](https://gitlab.com/runarberg/trompt/badges/master/build.svg)](https://gitlab.com/runarberg/trompt/commits/master)

Trompt
======

> Prompt your users with style

[Documentation](https://docs.rs/trompt)

*Trompt* aims to be a fully featured simple to use prompting libarary
for rust.

To get started add…

```toml
[dependencies]
trompt = "0.0.4"
```

…to your `Cargo.toml`, and…

```rust
extern crate trompt;
```

…at the top level of your crate.

From now on you can prompt your users using the `trompt::Trompt`
struct.


### Example ###

```rust
extern crate trompt;

use trompt::Trompt;

fn main() {
    let usr = Trompt::stdout()
        .required()
        .prompt("Username: ");

    let pwd = Trompt::stdout()
        .silent()
        .min_len(8)
        .prompt("Password: ");

    let is_sure = Trompt::stdout()
        .confirm("Are you sure [yn]? ");

    println!(
        "{}:{}, {}",
        usr.unwrap(),
        pwd.unwrap(),
        if is_sure.unwrap() { "is sure" } else { "is unsure" },
    );
}
```
