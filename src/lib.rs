//! A simple utility to prompt users of your CLI app.
//!
//! ## Examples
//!
//! ```
//! extern crate trompt;
//!
//! use trompt::Trompt;
//!
//! fn main() {
//!     // Normally you would want to something like this:
//!     //
//!     // ```
//!     // let input = std::io::stdin();
//!     // let output = std::io::stdout();
//!     // ```
//!     //
//!     // Or the equivalent:
//!     //
//!     // ```
//!     // let _prompter = Trompt::stdout();
//!     // ```
//!     //
//!     // But for now we will use cursors instead of stdin, and stdout.
//!
//!     use std::io::Cursor;
//!
//!     // Normally this would come from the user after they see the
//!     // prompt.
//!
//!     let input = Cursor::new(b"tupac");
//!     let output = Cursor::new(Vec::new());
//!
//!     // Here the user is asked for a username.
//!
//!     let username = Trompt::new(input, output)
//!         .required()
//!         .prompt("Username: ");
//!
//!     assert_eq!(username, Ok("tupac".to_string()));
//!
//!     // Same as prompt, but the console will not echo the user
//!     // input. Now we use a shorthand to write to stdout.
//!
//!     let password = Trompt::stdout()
//!         .silent()
//!         .min_len(8)
//!         .prompt("Password: ");
//!
//!     // We can use `confirm` to ask a question.
//!
//!     let input = Cursor::new(b"YES");
//!     let output = Cursor::new(Vec::new());
//!
//!     let confirmed = Trompt::new(input, output)
//!         .confirm("Are you sure [yn]? ");
//!
//!     assert_eq!(confirmed, Ok(true));
//! }
//! ```

extern crate libc;
extern crate termios;

use std::cmp::PartialEq;
use std::io::{self, Read, Write};
use std::string;


#[derive(Debug, PartialEq)]
pub enum ValidationError {
    Absent,
    TooLong,
    TooShort,
    UnexpectedInput(String),
    Other(String),
}

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Validation(ValidationError),
    FromUtf8(string::FromUtf8Error),
}

impl PartialEq for Error {
    fn eq(&self, other: &Error) -> bool {
        use Error::{Io, Validation};
        match (self, other) {
            (&Validation(ref a), &Validation(ref b)) => a == b,
            (&Io(ref a), &Io(ref b)) => a.kind() == b.kind(),
            _ => false,
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(err: string::FromUtf8Error) -> Error {
        Error::FromUtf8(err)
    }
}

impl From<ValidationError> for Error {
    fn from(err: ValidationError) -> Error {
        Error::Validation(err)
    }
}

pub type Result<T> = std::result::Result<T, Error>;
type ValidationResult<T> = std::result::Result<T, ValidationError>;

/// The state of the prompt that we send to the user. It is general on
/// any `input` and `output` that implement `Read` and `Write`
/// respectively.
///
/// ## Examples
///
/// Create prompter that prompts for a username on stdout awaiting a
/// response on stdin:
///
/// ```no_run
/// use trompt::Trompt;
/// let username = Trompt::stdout().prompt("username: ");
/// ```
///
/// Similarly create a prompter that writes to stderr instead, and
/// doesn’t echo the user input:
///
/// ```no_run
/// use trompt::Trompt;
/// let password = Trompt::stderr().silent().prompt("password: ");
/// ```
pub struct Trompt<R: Read, W: Write> {
    input: R,
    output: W,
    message: String,
    silent: bool,
    validators: Vec<Box<Fn(&str) -> ValidationResult<()>>>,
}

impl<R: Read, W: Write> Trompt<R, W> {
    /// Start a new prompter with default values.
    ///
    /// ## Examples
    ///
    /// ```no_run
    /// use std::io::Cursor;
    /// use trompt::Trompt;
    ///
    /// let input = Cursor::new(vec![]);
    /// let output = Cursor::new(vec![]);
    ///
    /// let prompter = Trompt::new(input, output);
    /// ```
    pub fn new(input: R, output: W) -> Trompt<R, W> {
        Trompt {
            input: input,
            output: output,
            message: "".to_owned(),
            silent: false,
            validators: vec![],
        }
    }

    /// Set the message before the input.
    ///
    /// ## Examples
    ///
    /// ```no_run
    /// use trompt::Trompt;
    ///
    /// let response = Trompt::stdout().message("Some prompt: ").send();
    /// ```
    pub fn message(&mut self, message: &str) -> &mut Trompt<R, W> {
        self.message = message.to_owned();
        self
    }

    /// Set to true if you want to hide the user input. For example
    /// for passwords.
    ///
    /// ## Examples
    ///
    /// ```no_run
    /// use trompt::Trompt;
    ///
    /// let password = Trompt::stdout().silent().prompt("password: ");
    /// ```
    pub fn silent(&mut self) -> &mut Trompt<R, W> {
        self.silent = true;
        self
    }

    /// Adds a validator to the input.
    ///
    /// Note calling this multiple times will call each validator in order.
    ///
    /// ## Examples
    ///
    /// ```
    /// use std::io::Cursor;
    /// use trompt::{Error, Trompt};
    /// use trompt::ValidationError::{Other, UnexpectedInput};
    ///
    /// let output = Cursor::new(vec![]);
    /// let input = Cursor::new(b"\
    ///     Jack & Jill\n\
    ///     Went up the hill\n\
    ///     To have fun @mordor\n\
    /// ".to_vec());
    ///
    /// let mut prompter = Trompt::new(input, output);
    ///
    /// prompter
    ///     .validate(move |s| if s.contains("&") {
    ///         Err(UnexpectedInput("&".into()))
    ///     } else {
    ///         Ok(())
    ///     })
    ///     .validate(move |s| if !s.contains("@") {
    ///         Err(Other("No @ present".into()))
    ///     } else {
    ///         Ok(())
    ///     });
    ///
    /// assert_eq!(prompter.send(), Err(Error::Validation(UnexpectedInput("&".to_string()))));
    /// assert_eq!(prompter.send(), Err(Error::Validation(Other("No @ present".to_string()))));
    /// assert_eq!(prompter.send(), Ok("To have fun @mordor".to_string()));
    /// ```
    pub fn validate<F>(&mut self, validator: F) -> &mut Trompt<R, W>
        where F: 'static + Fn(&str) -> ValidationResult<()>
    {
        self.validators.push(Box::new(validator));
        self
    }

    /// Set a limit on the maximum number of characters in the
    /// response.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::{Error, Trompt, ValidationError};
    ///
    /// let input = std::io::Cursor::new(b"This is too long");
    /// let output = std::io::Cursor::new(Vec::new());
    ///
    /// let too_long = Trompt::new(input, output)
    ///     .max_len(5)
    ///     .prompt("Be precise (max 5): ");
    ///
    /// assert_eq!(
    ///     too_long.unwrap_err(),
    ///     Error::Validation(ValidationError::TooLong)
    /// );
    /// ```
    pub fn max_len(&mut self, max: usize) -> &mut Trompt<R, W> {
        self.validate(move |response| if response.chars().count() > max {
            Err(ValidationError::TooLong)
        } else {
            Ok(())
        });
        self
    }

    /// Set a limit on the minimum number of characters in the
    /// response.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::{Error, Trompt, ValidationError};
    ///
    /// let mut input = std::io::Cursor::new(b"too short\n");
    /// let mut output = std::io::Cursor::new(Vec::new());
    ///
    /// let too_short = Trompt::new(&mut input, &mut output)
    ///     .min_len(12)
    ///     .prompt("Be explicit (min 12): ");
    ///
    /// assert_eq!(
    ///     too_short.unwrap_err(),
    ///     Error::Validation(ValidationError::TooShort)
    /// );
    /// ```
    pub fn min_len(&mut self, min: usize) -> &mut Trompt<R, W> {
        self.validate(move |response| if response.chars().count() < min {
            Err(ValidationError::TooShort)
        } else {
            Ok(())
        });
        self
    }

    /// Disallow responding with an empty string.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::{Error, Trompt, ValidationError};
    ///
    /// let mut input = std::io::Cursor::new(b"\n");
    /// let mut output = std::io::Cursor::new(Vec::new());
    ///
    /// let too_short = Trompt::new(&mut input, &mut output)
    ///     .required()
    ///     .prompt("Say something (required): ");
    ///
    /// assert_eq!(
    ///     too_short.unwrap_err(),
    ///     Error::Validation(ValidationError::Absent)
    /// );
    /// ```
    pub fn required(&mut self) -> &mut Trompt<R, W> {
        self.validate(move |response| if response == "" {
            Err(ValidationError::Absent)
        } else {
            Ok(())
        });
        self
    }

    /// Send the request to the user, and get the response.
    ///
    /// Halts exeution until input receives a new line character or an
    /// EOF.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::Trompt;
    ///
    /// let input = std::io::Cursor::new(b"bar");
    /// let mut output = std::io::Cursor::new(Vec::new());
    ///
    /// let response = Trompt::new(input, &mut output).message("foo").send();
    ///
    /// assert_eq!(output.into_inner(), b"foo");
    /// assert_eq!(response, Ok("bar".to_string()));
    /// ```
    pub fn send(&mut self) -> Result<String> {
        write!(self.output, "{}", self.message)?;
        self.output.flush()?;

        let should_restore = if self.silent { Some(silent()?) } else { None };
        let response = self.input
            .by_ref()
            .bytes()
            .take_while(|&ref byte| {
                if let &Ok(byte) = byte {
                    byte != 0xA && byte != 0xD
                } else {
                    false
                }
            })
            .collect::<io::Result<Vec<u8>>>();

        if let Some(restore) = should_restore {
            restore()?;
        }

        let response = String::from_utf8(response?)?;

        (*self.validators).into_iter()
            .fold(Ok(()), |result, f| result.and_then(|()| f(&response)))
            .map(|()| response)
            .map_err(|err| err.into())
    }

    /// A helper. Same as `.message(message).send()`.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::Trompt;
    ///
    /// let input = std::io::Cursor::new(b"bar");
    /// let mut output = std::io::Cursor::new(Vec::new());
    ///
    /// let response = Trompt::new(input, &mut output).prompt("foo");
    ///
    /// assert_eq!(output.into_inner(), b"foo");
    /// assert_eq!(response, Ok("bar".to_string()));
    /// ```
    pub fn prompt(&mut self, message: &str) -> Result<String> {
        self.message(message).send()
    }

    /// A helper for retrieving confirmation from the user. Maps `y`
    /// and `yes` case insensitively to true and `n` and `no` to
    /// false.
    ///
    /// ## Examples
    ///
    /// ```
    /// use trompt::Trompt;
    ///
    /// let input = std::io::Cursor::new(b"YES");
    /// let output = std::io::Cursor::new(Vec::new());
    ///
    /// let result = Trompt::new(input, output).confirm("should I?");
    ///
    /// assert_eq!(result.unwrap(), true);
    /// ```
    pub fn confirm(&mut self, message: &str) -> Result<bool> {
        self.prompt(message).and_then(|result| match result.to_lowercase().as_str() {
            "y" | "yes" | "1" => Ok(true),
            "n" | "no" | "0" => Ok(false),
            s @ _ => Err(Error::Validation(ValidationError::UnexpectedInput(s.into()))),
        })
    }
}

impl Trompt<std::io::Stdin, std::io::Stdout> {
    /// Start a new prompter from stdin that writes to stdout.
    ///
    /// ## Examples
    ///
    /// ```no_run
    /// use trompt::Trompt;
    ///
    /// let _ = Trompt::stdout().prompt("This is stdout speaking: ");
    /// ```
    pub fn stdout() -> Trompt<std::io::Stdin, std::io::Stdout> {
        Trompt::new(std::io::stdin(), std::io::stdout())
    }
}

impl Trompt<std::io::Stdin, std::io::Stderr> {
    /// Start a new prompter from stdin that writes to stderr.
    ///
    /// ## Examples
    ///
    /// ```no_run
    /// use trompt::Trompt;
    ///
    /// let _ = Trompt::stderr().prompt("This is stderr speaking: ");
    /// ```
    pub fn stderr() -> Trompt<std::io::Stdin, std::io::Stderr> {
        Trompt::new(std::io::stdin(), std::io::stderr())
    }
}


#[cfg(not(test))]
fn silent() -> io::Result<Box<Fn() -> io::Result<()>>> {
    use libc::STDIN_FILENO;
    use termios::{ECHO, ECHONL, TCSANOW, Termios, tcsetattr};

    let mut term = Termios::from_fd(STDIN_FILENO)?;
    let orig_term = term;

    // Don't echo anything except new lines
    term.c_lflag &= !ECHO;
    term.c_lflag |= ECHONL;

    tcsetattr(STDIN_FILENO, TCSANOW, &term)?;

    Ok(Box::new(move || tcsetattr(STDIN_FILENO, TCSANOW, &orig_term)))
}

#[cfg(test)]
// TODO: Find out how to make this testable.
fn silent() -> io::Result<Box<Fn() -> io::Result<()>>> {
    Ok(Box::new(move || Ok(())))
}

#[cfg(test)]
mod tests {
    use Trompt;
    use std::io::Cursor;

    #[test]
    fn defaults() {
        let mut input = Cursor::new(Vec::new());
        let mut output = Cursor::new(Vec::new());
        let prompter = Trompt::new(&mut input, &mut output);

        assert_eq!(prompter.message, "");
        assert_eq!(prompter.silent, false);
        assert_eq!(prompter.validators.len(), 0);
    }

    #[test]
    fn message() {
        let mut input = Cursor::new(Vec::new());
        let mut output = Cursor::new(Vec::new());

        let mut prompter = Trompt::new(&mut input, &mut output);
        prompter.message("foo");

        assert_eq!(prompter.message, "foo");
    }

    #[test]
    fn silent() {
        let mut input = Cursor::new(Vec::new());
        let mut output = Cursor::new(Vec::new());

        let mut prompter = Trompt::new(&mut input, &mut output);
        prompter.silent();

        assert_eq!(prompter.silent, true);
    }

    #[test]
    fn validate_pushes_to_validators() {
        let mut input = Cursor::new(Vec::new());
        let mut output = Cursor::new(Vec::new());

        let mut prompter = Trompt::new(&mut input, &mut output);

        prompter.validate(move |_| Ok(()));
        assert_eq!(prompter.validators.len(), 1);

        prompter.validate(move |_| Err(::ValidationError::Other("Error".into())));
        assert_eq!(prompter.validators.len(), 2);
    }

    #[test]
    fn validate_validates_response() {
        use Error::Validation;
        use ValidationError::UnexpectedInput;

        let mut input = Cursor::new(b"no & here\n");
        let mut output = Cursor::new(Vec::new());

        let illegal = Trompt::new(&mut input, &mut output)
            .validate(move |s| if s.contains("&") {
                Err(::ValidationError::UnexpectedInput("&".into()))
            } else {
                Ok(())
            })
            .prompt("");

        let mut input = Cursor::new(b"But this is fine\n");
        let ok = Trompt::new(&mut input, &mut output)
            .validate(move |s| if s.contains("&") {
                Err(::ValidationError::UnexpectedInput("&".into()))
            } else {
                Ok(())
            })
            .prompt("");

        assert_eq!(illegal.unwrap_err(),
                   Validation(UnexpectedInput("&".into())));
        assert!(ok.is_ok());
    }

    #[test]
    fn validate_can_be_called_more_than_once() {
        use Error::Validation;
        use Result;
        use ValidationError::{TooLong, UnexpectedInput};

        fn respond(response: &'static [u8]) -> Result<String> {
            let mut input = Cursor::new(response);
            let mut output = Cursor::new(Vec::new());
            Trompt::new(&mut input, &mut output)
                .max_len(16)
                .validate(move |s| if s.contains("&") {
                    Err(::ValidationError::UnexpectedInput("&".into()))
                } else {
                    Ok(())
                })
                .prompt("")
        };

        let too_long = respond(b"This will not validate\n");
        let contains_ampersand = respond(b"& is illegal\n");
        let ok = respond(b"fine\n");

        assert_eq!(too_long.unwrap_err(), Validation(TooLong));
        assert_eq!(contains_ampersand.unwrap_err(),
                   Validation(UnexpectedInput("&".into())));
        assert!(ok.is_ok());
    }

    #[test]
    fn max_len() {
        use Error::Validation;
        use ValidationError::TooLong;

        let mut input = Cursor::new(b"This is too long\n");
        let mut output = Cursor::new(Vec::new());

        let illegal = Trompt::new(&mut input, &mut output)
            .max_len(5)
            .prompt("");

        let mut input = Cursor::new(b"fine\n");
        let ok = Trompt::new(&mut input, &mut output)
            .max_len(5)
            .prompt("");

        assert_eq!(illegal.unwrap_err(), Validation(TooLong));
        assert!(ok.is_ok());
    }

    #[test]
    fn min_len() {
        use Error::Validation;
        use ValidationError::TooShort;

        let mut input = Cursor::new(b"Too short\n");
        let mut output = Cursor::new(Vec::new());

        let illegal = Trompt::new(&mut input, &mut output)
            .min_len(12)
            .prompt("");

        let mut input = Cursor::new(b"Is this many enough for you?\n");
        let ok = Trompt::new(&mut input, &mut output)
            .min_len(12)
            .prompt("");

        assert_eq!(illegal.unwrap_err(), Validation(TooShort));
        assert!(ok.is_ok());
    }

    #[test]
    fn required() {
        use Error::Validation;
        use ValidationError::Absent;

        let mut input = Cursor::new(b"\n");
        let mut output = Cursor::new(Vec::new());

        let illegal = Trompt::new(&mut input, &mut output)
            .required()
            .prompt("");

        let mut input = Cursor::new(b"This is fine\n");
        let ok = Trompt::new(&mut input, &mut output)
            .required()
            .prompt("");

        assert_eq!(illegal.unwrap_err(), Validation(Absent));
        assert!(ok.is_ok());
    }

    #[test]
    fn send_message_to_output() {
        let mut input = Cursor::new(Vec::new());
        let mut output = Cursor::new(Vec::new());

        let _ = Trompt::new(&mut input, &mut output).message("foo").send();

        assert_eq!(output.into_inner(), b"foo");
    }

    #[test]
    fn send_collects_from_input() {
        let mut input = Cursor::new(b"bar\n");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).send();

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "bar");
    }

    #[test]
    fn send_collects_a_line() {
        let mut input = Cursor::new(b"bar\nbaz");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).send();

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "bar");
        assert_eq!(input.position(), 4);
    }

    #[test]
    fn send_collects_until_eof() {
        let mut input = Cursor::new(b"bar");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).send();

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "bar");
    }

    #[test]
    fn prompt() {
        let mut input = Cursor::new(b"bar\n");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).prompt("foo");

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "bar");
        assert_eq!(output.into_inner(), b"foo");
    }

    #[test]
    fn prompt_with_silent() {
        let mut input = Cursor::new(b"bar\n");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).silent().prompt("foo");

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "bar");
        assert_eq!(output.into_inner(), b"foo");
    }

    #[test]
    fn confirm() {
        let mut input = Cursor::new(b"y\n");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).confirm("should I?");

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), true);
        assert_eq!(output.into_inner(), b"should I?");
    }

    #[test]
    fn confirm_case_insensitive() {
        let mut input = Cursor::new(b"No\n");
        let mut output = Cursor::new(Vec::new());

        let result = Trompt::new(&mut input, &mut output).confirm("should I?");

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), false);
        assert_eq!(output.into_inner(), b"should I?");
    }
}
